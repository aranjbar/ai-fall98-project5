class Neuron(DifferentiableElement):
	# ...
    def compute_output(self):
        z = 0
        for inp, weight in zip(self.get_inputs(), self.get_weights()):
            z += inp.output()*weight.get_value()
        return sigmoid(z)

    def compute_doutdx(self, elem):
        result = 0
        dSigmoid = self.output()*(1 - self.output())

        if self.has_weight(elem):
            idx = self.my_weights.index(elem)
            inp_value = self.get_inputs()[idx].output()
            result = dSigmoid*inp_value
        else:
            for i, weight in enumerate(self.get_weights()):
                if self.isa_descendant_weight_of(elem, weight):
                    result += weight.get_value()*(self.get_inputs()[i].dOutdX(elem))
            result *= dSigmoid

        return result
