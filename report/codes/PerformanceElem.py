class PerformanceElem(DifferentiableElement):
	# ...
    def output(self):
        return -0.5*(self.my_desired_val - self.my_input.output())**2

    def dOutdX(self, elem):
        return (self.my_desired_val - self.my_input.output())*self.my_input.dOutdX(elem)
 
